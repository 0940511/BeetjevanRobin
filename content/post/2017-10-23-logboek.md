---
title: Logboek
subtitle: CMD jaar 1
date: 2017-10-23
categories: ["logboek"]
tags: ["logboek", "day-to-day"]
---
*30 augustus 2017* > groepsnaam bedacht, groepslogo en groepsobject gemaakt.

*31 augustus 2017* > uitleg over eerste design challenge, opdracht ontvangen, hoorcollege over ontwerpproces en games.

*1 september 2017* > Eerste ideeënstroom voor opdracht+ eisen stellen wanneer eerste deliverables af moeten.

*4 september 2017*  > Eerste schooldag, teamcaptain gekozen en verder gewerkt aan blog en eerste stappen van deliverables afgemaakt.  Eerste twee punten van de deliverables zijn af. Groepsposter gemaakt, tijdens studie coaching workshop. Website bijgewerkt, www.littlebitofrobin.com/

*5 september 2017* > Hoorcollege Design theorie, HC1 - het ontwerpproces. Thuis verder gewerkt aan het onderzoek en moodboard.

*6 september 2017* > werken aan design challenge 1, workshop blog maken, onderzoek afronden en moodboards af, blog verder bijwerken en werken aan 'about me' profiel.  *ALLES OVERGEPLAATST VAN DE BOVENGENOEMDE WEBSITE NAAR STACKEDIT :(*

*7 september* >  Werkcollege,  Afspraak met de decaan gehad, verder gewerkt aan de blog,  verder het onderzoek uitgewerkt en begonnen aan de eerste ideeën van de game. 

*8 september* >  Interviews gehouden met eerste jaars CMD studenten. Blog bijgewerkt.  moodboard gemaakt

*9 september* > Het spel verder uitgewerkt. Blog bijgewerkt. (eigen onderzoek af)

*10 september* > Spel uitgewerkt. (je eigen spel af)

*11 september* > Spel analyse gedaan, prototype af. 

*12 september* > Hoorcollege verbeelden, samenvatting gemaakt. 

*13 september* > Presentatie gemaakt. workshop prototyping gehad. 

*14 september* > Dit ben ik profiel gemaakt 1.0, werkcollege gehad

*15 september* > Iteratie 1 ingeleverd op n@tschool, leverde veel groepsproblemen op. workshop prototyping gehad. 

*18 september* > presentatie gehad, feedback gekregen. 

*19 september* > hoorcolleges prototyping, samenvatting gemaakt.

*20 september* > nieuwe iteratie gekregen en planning gemaakt. samenvatten onderzoek vorige iteratie. Onderzoek online gaming gedaan.

*21 september* > Research Rotterdam, mensen geïnterviewd met Puck. Creatieve technieken gelezen uit het boek. Werkcollege gehad

*22 september* > moodboards verbeteren. Interview met puck in de stad gedaan. workshop prototyping gehad. 

*25 september* > moodboards gemaakt. planning lesweek 5 gemaakt. nieuwe concepten gemaakt + nieuw spel bedacht.

*26 september* > Hoorcolleges onderzoeken, samenvatting gemaakt. 

*27 september* > uitleg dit ben ik profiel 2.0 --> gemaakt en ingeleverd.

*28 september* > werkcollege gehad. Gewerkt aan het peperprototype. 

*29 september* > nulmeting tekenen.  workshop prototyping gehad. ingeschreven voor 3 keuze vakken en 2 op de wachtlijst.

*2 oktober* > planning week 6 gemaakt, feedback gevraagd voor nieuwe game bij bob.

*3 oktober* > presentatie gemaakt + alles ingeleverd --> ging beter dan iteratie 1.

*4 oktober*> presentaties met alumni, feedback gekregen en opgeschreven. 

*5 oktober* > nulmetingen Engels en Nederlands gehad, was erg makkelijk.

*9 oktober* > nieuwe iteratie gekregen en planning gemaakt. opnieuw feedback gevraagd aan de alumni en aan bob.  Personal Challenge bedacht en opgestuurd naar Gerhard. 

*10 oktober* > 2 minigames verzonnen, 2 escape room dingen verzonnen en attributen die verzameld moeten worden.  Ingeschreven voor tools for design periode 2 indesign op dinsdag avond en illustrator op donderdag ochtend. 

*11 oktober* > minigames vastgelegd en verder aan overige planning gewerkt. 

*12 oktober* > Tentamen eerste blok, spiekbrief afgemaakt. 

*13 oktober* > opnieuw gitlab aangemaakt. 

*14 oktober* > onderzoek begonnen aan personal challenge, wat zijn de karakters en wat zijn de planningen.

*16 oktober* > filmpje gemaakt voor de game.

*20 oktober* > voorwerpen gemaakt voor in de mini-escape room.

*23 oktober* > Prototype afgemaakt, minigames afgemaakt, regels gechecked, spelanalyse uitgevoerd, blog op gitlab gezet.

*24 oktober* > verder gewerkt aan blog, spelanalyse uitgewerkt, fotos gemaakt en alles afgechecked.
