---
title: Onderzoeken
subtitle: Mijn gedachtes en ideeen en onderzoeken van de eerste design challenge.
date: 2017-10-23
categories: ["design challenge 1"]
tags: ["onderzoek", "idee", "gedachten"]
---

**Offline spellen onderzoek

Een gezelschapsspel is een spel dat wordt gespeeld voor de gezelligheid, om met bekenden en familie de tijd te verdrijven. Vaak zijn dit bordspellen (zoals Monopoly), maar ook sjoelen en sommige kaartspellen worden als gezelschapsspel aangemerkt. Omdat het bij een gezelschapsspel om de gezelligheid gaat, wordt er niet om een prijs of inzet gespeeld.
Een bordspel is een spel dat op een tevoren gemarkeerd oppervlak (spelbord) wordt gespeeld. Er wordt dan gebruikgemaakt van stukken, stenen of fiches. Sommige bordspelen zijn pure kansspelen (er wordt dan bijvoorbeeld met een dobbelsteen gerold). Andere zijn strategisch, zoals schaken. Een combinatie van strategie en kans komt ook veel voor.

Dobbelen is het spelen met dobbelstenen. Het spel zelf wordt ook wel dobbelspel genoemd. Dobbelen is dus het toepassen van een toevalsgenerator. Ergens om dobbelen wordt dan ook als een eenvoudige manier gezien om een onoplosbaar lijkend geschil op te lossen. Als er om geld gespeeld wordt is het een eenvoudig kansspel.
Het woord dobbelen wordt ook overdrachtelijk gebruikt voor het doen van geldelijke transacties waarbij er sprake is van een kans op winst en een risico van verlies, het speculeren.

Onder kaartspel wordt zowel het kaartmateriaal (meervoud: kaartspellen) als de activiteit (meervoud: kaartspelen) verstaan. Kaartspelen zijn spelen die gespeeld worden met een set (speel)kaarten (dit in tegenstelling tot de bordspelen). De kaartspelen worden allereerst onderverdeeld in spelen die de welbekende speelkaarten (Harten, Ruiten, Klaveren en Schoppen) gebruiken en de overige spelen, die eigen sets kaarten hebben. De eerste groep wordt verder onderverdeeld in spelen met een kaartspel bestaande uit 32, 52 of uit 54 kaarten

Een kansspel of hazardspel is een speltype waarbij het toeval bepaalt of een speler een bepaalde prijs wint.
Een kansspel kan niet op een tactische manier worden gewonnen. Het hebben van 'geluk' speelt derhalve een dominante rol. Veel kansspelen zijn gebaseerd op het gokken van het juiste getal of combinatie van getallen die op een bepaald moment worden getrokken. Zo'n spel noemt men een loterij.
Bij een spel waarbij de deelnemer zelf een of meer nummers kiest zonder directe controle of iemand anders datzelfde nummer of diezelfde nummers heeft gekozen moet een prijs in voorkomend geval soms gedeeld worden. Zelfs binnen één kansspel geldt dit soms voor een deel van de prijzen wel, voor een deel niet.
Bij een commercieel kansspel is de verwachtingswaarde van de winst voor de speler kleiner dan de inzet. Wanneer men zo'n kansspel toch aantrekkelijk vindt spreekt men wel van risico-preferentie. Wel is het maximale verlies vaak van tevoren duidelijk: het bedrag van de inzet. Ook is het maximale verlies vaak klein vergeleken met de maximale winst.
Gokken of wedden is een bezigheid waarbij iemand probeert de uitslag te voorspellen van een spel, wedstrijd, toernooi enzovoort. De uitslag van het spel is geheel of gedeeltelijk toevallig. De uitkomst van de meeste gokspelen wordt niet door kennis of bekwaamheid bepaald. Uitzonderingen daarop zijn bijvoorbeeld poker en blackjack, waarbij technieken en tactieken een rol spelen. Ook bij bijvoorbeeld sportweddenschappen zijn deze dingen van belang, evenals kennis en het kunnen inschatten van kansen. Ook hier gaat het dus niet om puur toevallige gebeurtenissen, zoals bij bijvoorbeeld roulette. Spelers zetten voor het spel een geldbedrag of iets van waarde in. Na de uitslag wordt er een winst aan de winnaar uitgekeerd.
Een puzzel is een soort spel of raadsel dat men als tijdverdrijf probeert op te lossen. Iemand die zich bezighoudt met het oplossen van puzzels noemt men een 'puzzelaar'.
Veel puzzels komen voort uit wiskundige of logistieke problemen. Andere, zoals schaakproblemen, komen voort uit bordspelen, en nog andere zijn gewoon ontworpen als breinbrekers.

Een raadspel is een spel waarbij de deelnemers iets moeten raden. Dat iets kan variëren van een woord, tot de titel van een boek of film, of wat een afbeelding voorstelt. Afhankelijk van de spelregels kan een deelnemer van de anderen winnen door voldoende goede antwoorden te geven.

WAAROM SPELEN? 

Mensen vinden dit leuker dan online omdat het mensen samenbrengt en het geeft gezelligheid. Het verbindt mensen, het brengt ze bij elkaar. Sommige spellen kunnen grote groepen bij elkaar brengen, zoals wedstrijden waarbij ze tegen elkaar spelen. Offline spelletjes zijn behalve gezelschapspellen ook spellen waarbij je iets kan leren. 
Sidenote: Sommige spelen kosten geld (verzamelen van materialen) en als dingen duurder worden, is het vaak minder interessant voor de spelers.

***DOELGROEP onderzoek***

STUDENTENSTAD= ROTTERDAM.
In Rotterdam is alles voor de student, uit enquete kwam dat Rotterdams thema een studentenstad moest zijn, wat biedt Rotterdam de studenten?
 
ONLINE GAMES= VERBINDEN MET ROTTERDAM
 
race tegen de klok en leer Rotterdam kennen door quizzen
OFFLINE GAMES= VERBINDEN MET ELKAAR
Samenwerken met elkaar vormt teambuilding en je leert elkaar kennen.
 

DOELGROEP GAMES= MET CREATIEVE OPLOSSINGEN EEN GEWENSTE SITUATIE CREEEREN
Spellen gebaseerd op CMD, dat wil zeggen van de huidige situatie een beter en gewenste situatie creëren  brug bouwen van lego.
 
