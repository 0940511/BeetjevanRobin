---
title: Blog Design Challenge 1
subtitle: Uitgebreider wat ik heb gedaan tijdens de eerste design challenge
date: 2017-10-23
categories: ["blog"]
tags: ["design challenge 1", "blog", "day-by-day"]
---
###31 augustus 2017 Uitleg hoe het jaar in zijn werk gaat.
####De design challenge, het ontwerpproces.
Voor de eerste design challenge is 8 weken de tijd. **De eerste deadline is 15 september 2017 om 17:00.** Dan moet alles op N@tschool staan. *(Hoe ik dat doe is nog onbekend!)*

Het ontwerpproces steekt simpel in elkaar en herhaalt zich voortdurend. Het is een cirkel die zich steeds herhaalt. Voor de ontwerpvragen is onderzoek doen (!) het belangrijkste. Hiermee krijg je een goed beeld van de situatie. Onderzoek kan op verschillende manier gedaan worden, door middel van fieldsearch en desksearch verzamel je de nodige informatie. Verder is het in leven en het in de schoenen kruipen van de gebruiker een belangrijke stap voor het onderzoek. ONDERZOEK = VRAGENSTELLEN.
Nadat een goed beeld is ontwikkeld, wordt er een plan van aanpak gemaakt. Dit is het concept van het onderwerp. Het concept maakt alles concreter. Door het concrete beeld dat we hebben kun je door naar de laatste stap van de eerste iteratie.
De laatste stap is het maken van een prototype. Dit geeft goed visueel weer wat het idee is van het ontwerp. Het prototype kan nu getest worden en er kan worde gekeken of het daadwerkelijk werkt. Hierna begint het proces opnieuw om verbeteringen en aanpassingen door te voeren.

*“Iets visueels kan nieuwe inzichten geven!”*

####Ontwerper?

Om een goede ontwerper te worden is het belangrijk dat er een goed evenwicht is tussen kennis, vaardigheden en talent. Om succesvol te worden moet je nieuwsgierig zijn. Kennis krijg je door design theory (week 7 tentamens), vaardigheden ontwikkel je door personal challenges (week 8 opdrachten) en talent is de passie voor het vak. Dit samen vormt je houding. **Je houding is bij CMD het allerbelangrijkste!!!** *Nogmaals onderzoek=vragenstellen.* Daarnaast is het belangrijk dat je feedback krijgt. FEEDBACK zorg voor vooruitgang. School is vanaf nu prioriteit nummer 1!

User centered design, dit is een van de vele termen die naar het hoofd gesmeten zal worden. Het is niet erg als het een onbekende term is, maar durft te vragen wat het betekent. Het went vanzelf. Zoals het woord als zegt ontwerp je voor de gebruiker, passend binnen de eisen en het budget van de opdrachtgever. *“Niet maken wat de opdrachtgever wil, maar wat de gebruiker nodig heeft!” *


###4 september 2017, Eerste schooldag uitleg en regelen.

Dingen die vandaag geregeld moeten worden:

Teamcaptain kiezen en regels bedenken.
Workshops uitzoeken
*Note to self*, regel een kussentje en alles met de decaan voor de fibromyalgie en ADHD. >  Afspraak regelen bij de decaan,
*Het is mogelijk om alle workshops te volgen, mits ze niet overlappen. Mocht ik dan sommige niet kunnen volgen kan je ze aanvragen. Er zijn drie dagen waaraan we moeten werken aan de design challenge, 2 zijn ingeroosterd en 1 niet. Op dit moment snap ik niet waarom die derde dag niet ook gewoon is ingeroosterd als we die zo hard nodig hebben, maar misschien is dit iets typisch van studeren en hbo. Een stap naar zelfstandigheid.  Het niveau van hbo en vwo is erg verrassend. Het is ineens een stuk makkelijker.*

*Persoonlijk vind ik de klassen splitsen prettiger, want dan is het minder chaotisch, maar misschien is dit een goede leermanier om met drukte om te gaan.*

Regels waar we op gekomen zijn;

-Houd het schoon
-Rekening met elkaar houden
-Dingen die na de eerste captainvergadering geregeld gaan worden (geprobeerd); kapstok, paraplubak, gezamenlijk lunchen 1x per week, muziek, studio versieren, tafels organiseren, facebookgroep voor deze studio, koffie en thee.

*Misschien is een kapstok een idee, te veel jassen in de winter.  Dit wordt geregeld, zelfs een paraplubak. Meer ruimte creëren. Het versieren van de ruimte en muziek is voor mij persoonlijk een bijzaak en niet nodig, maar het is niet storend. Koffie en thee is wel echt fijn.*

###Studie coaching, 4 september 2017.

inventaris team bijwerken. > vorige opleiding, achtergrond.

Teamposter maken;
-Diversiteit.
-Wie zijn we ? kwaliteiten?
-Ambities > beter ‘ zijn’ , goede samenwerking
-Eigen ambities samenvoegen tot 1.
-Team afspraken > regels, cultuur bepalen, ceremonie bedenken, klasvergadering.

*NOTE, maak thuis scrumboard om mijn vorderingen bij te houden!!*

###Studiecoaching, 6 september 2017. 

wat zit er allemaal in een teamverband en hoe werken dingen samen? Vaak slechteren communicatie als tijdsdruk wordt toegepast op de driehoek. 

**Het ontwerpproces**

`Opdracht design challenge 1.
Ontwerp een spel dat gebruikt maakt van een online interactief element dat in de introweek van 2018 studenten in Rotterdam met elkaar en met de stad Rotterdam verbindt.`

***Vragen bij design challenge 1, iteratie 1:***

Wat is een spel?
Wat zijn spellen voor deze doelgroep en waarom?
Hoe komt het online aspect terug en het offline aspect?
Hoe verbinden we de mensen met elkaar? Teambuilding?
Hoe verbinden we ze met de stad? Pokémon go/ontdekkingstour?
Is het leuk om de stad te leren kennen door middel van geschiedenis?
Opdrachtgever/ ontwerpen voor HR. (wat houdt HR in?)

Welke doelgroep van HR? (Pabo, WDK, economie, ICT, CMD of foreign students)

Welke Rotterdams thema moet terugkomen? (Wat zijn de mogelijke thema’s)

Het spel moet zowel online als offline
Eerste versie klaar op 15 september 2017.
3 iteraties, met 3 keer het ontwerpproces.
Week 6 is feedback, week 8 is definitieve versie + presenteren aan de opdrachtgever.
 

 

***1 september 2017, eerste indruk design challenge.***

Het eerste waar ik aandacht:

Pokémon GO
Vragenlijst, die mensen offline kunnen invullen en online met elkaar verbindt. (Dezelfde interesses)
Challenges/ uitdagingen voor punten of level up.
Teams vormen, met bijvoorbeeld door pc gekozen mensen.
Puntensysteem en ranglijsten met alle teams.
Geschiedenis van Rotterdam leren, doormiddel van speurtocht idee.
Opdrachten op verschillende plekken + foto als eindresultaat insturen à docenten account die punten kunnen toe dienen.
Nerve idee.

*Wat er maandag geregeld moet worden: (gedaan op maandag 4 september 2017)*

Eerste 2 punten van de deliverables iteratie 1. > Inventaris team, team afspraken en gezamenlijke doelen. > Planning en taakverdeling
Wat er voor maandag gemaakt moet zijn:

Dit ben ik profiel. (Bladzijde 1) *(ik heb op dit moment (maandag 4 september 2017) geen idee wat ik moet invullen), > later vul ik hem aan.
Onderzoek mee, foto’s/ ideeën moodboard. (Naar andere dag verplaatst, voor woensdag 6 september 2017 af)* 
 

***4 september 2017, onderzoek doelgroep.***

Ik heb nagedacht over welke vragen ik wil stellen en ze opgesteld per categorie. Om zo makkelijker te verdelen en taken te krijgen. Beetje overzicht in de chaos in mijn hoofd. Ik vind het vooral lastig om een thema te bepalen.

****Doelgroep;****

Gekozen voor CMD als doelgroep.

wat maakt een spel leuk voor CMD studenten?
Wat wil je als aankomend student weten over Rotterdam?
Hoeveel mensen zou je in 1 keer als nieuwe student ontmoeten ? wat is te veel ?
Hoe sla je alle informatie op die je hebt gekregen? is een “database” zoals Pokemon handig?
Wat wil je weten van de nieuwe mensen die je gaat ontmoeten?
Wat is het eindgoal ? Moet er een beloning zijn na het spel ? Zoals gratis studiepunt, eten of gewoon alleen de eer? Wat is het doel?

****Rotterdamse thema’s****

Restaurants
Geschiedenis
Winkelen
Uitgaan/night life
Musea
Sport
Openbaar vervoer

****Online aspect****

Routes lopen naar uitdagingen in Rotterdam zelf.
Gegevens uitwisselen met mede studenten. (studenten verzamelen in je ‘database’)
Awards krijgen en tegen teams strijden.
Kennis/ multiple choice vragen.
Matches en groepjes maken.

****Offline aspect****

Eenmaal op een plek aangekomen, in het echt een opdracht/challenge/spel spellen.
Challenge en opdrachten uitvoeren die door, bijvoorbeeld peercoaches, wordt uitgelegd.
Persoonlijk profiel/ vragenlijst invullen (die wel online moet worden opgestuurd)
Avatar aanmaken.

****Mensen verbinden****

Matches door middel van vragenlijst. (vragenlijst lijkend op liever een hond of een kat).
Speeddaten.
Vele groepswissels.
Groepjes splitsen en met zijn tweeën naar een volgend punt.

****De game****

 Hoe maak je het leuk?
Regels ? Hoe simpel kunnen we het maken?
Rewardsysteem werkt dat goed genoeg?
Online en reallife matchen?
Wat is het doel? is er alleen een einddoel of ook kleinere doelen?
Keuzes ? welke en wat voor?
Wat is de ervaring, waardoor mensen het leuk vinden en willen spelen?
Wat zijn de consequenties bij een goed en een fout antwoord?
Is er een tijdslimiet? Net als bij de game in de film Nerve?
Hoe groot is de Magic Circle?
Voor elke soort speler wat thuis?
Route liever makkelijk en vooruitgestippeld in de game zelf? Of zelf uitzoeken door middel van Google Maps te gebruiken?
 

****Onderzoek visueel maken?****

Collage.
Moodboard.
Kernwoorden.
Foto’s, beeldmateriaal, screenshots.
Op A3 papier.
Breng de kern naar voren.
Onderzoek uitvoeren;

Enquête gemaakt om te kijken wat de doelgroep wilt.