---
title: Hoorcolleges Aantekeningen
subtitle: Design Challenge 1, Het ontwerpproces
date: 2017-10-23
categories: ["hoorcolleges"]
tags: ["hoorcollege", "ontwerpproces","verbeelden","prototyping", "onderzoeken", "Designchallenge"]
---

***Hoorcollege 1, Het ontwerpproces***


****What is design?****   

Synoniem voor design, is ook wel gebruiksvriendelijkheid, prestatie, mooiheid. Maar ook producten met bepaalde esthetische waarden, bepaalde merken of een bepaalde lifestyle.

dit is hoe een gemiddelde mens design ervaart!

Voor mij is design , vormgeving, aandacht voor kleuren.

Design is everything artificial, man-made things and systems. 
–> dus niet gemaakt door moedernatuur.

`Design is to design a design to produce a design.  ~ John Hasket.`
In de quote zitten alle vier types voor design,

Beroepspraktijk, designveld, zoals architectuur, interieur, lifestyle etc.
Actie(creatie), werkwoord, actie van concept > resultaat.
Concept (idee), gedachte, doelstelling, wat wil je waar maken?
Resultaat (ding), tas, winkel, advies/service.
–> gekoppeld aan behoeftes van de mens + technologie (Altijd veranderend).

Vereist veel creativiteit.

Design is geen kunst.

Design is gemaakt voor de gebruikers en objectief/extern. Terwijl kunst niet gekoppeld is aan de gebruikers, maar gemaakt voor kijkers  en het is subjectief/intern.

Ontwerpen hoef je niet zelf altijd mooi/leuk te vinden. Als het maar werkt voor de gebruikers.

Design = ability, vaardigheid/competenties. Het vermogen om een handeling bekwaam uit te voeren.

`Design is the ability to imagine that-which-does-not-yet-exsist to make it appear in concrete form as a new purposed addition to the new world ~ Nigel Cross.`
`We all design when we plan for something new to happen. ~ Nigel Cross`
Nigel cross zegt iedereen kan ontwerpen. new -> change/ verandering. Soorten veranderingen > Vernieuwingen; nieuwe situatie beter maken, omdat de oude situatie niet meer leefbaar is.

`Everyone designs who devices courses of action aimed at changing existing situations into preferred acts.  ~ Herbert Simon`


**Levels of design**

Hoe snel je bent of hoe goed je bent, ligt aan je vaardigheden, hoe meer je oefent des te beter je wordt.



Design is een proces. Van huidige situatie naar nieuw.

Proces is het verloop van geleidelijke veranderingen beschouwd in stappen of volgorde.





Dit iteratieproces is het proces voor de design challenges, itereren is het telkens herhalen van het proces.

Mogelijke andere design processen;

Stanford Process.

Ideo Process

`Design is one of the most complex ways of problem solving ~  Kees Dorst.`

***Design problems***

Het grootste design probleem is de voortdurende veranderingen van de toekomst, samen met de voortdurende veranderingen van de behoeftes van de mens (gebruikers).  (constante veranderingen!!)

Dit probleem zorgt voor:

–> Veel onzekerheid .

–> Veel onbekendheden.      Dit samen is dus onvoorspelbaar = Wicked Problems

Proces is nooit een rechte lijn!
Als ontwerper moet je geduldig zijn met je doelgroep. Verder moet je slim zijn en een echte detective worden.  Stand up to the challenge !



Als de problemen te moeilijk of onoverzichtelijk zijn, moet je ze onder verdelen in sub problemen en sub oplossingen. Soms brengen alle sub oplossingen en problemen samen een conclusie voor het totale probleem en de totale oplossing.

`By trying to understand the problem and experimenting with possible solutions designing becomes a learning process. ~ Kees Dorst.`