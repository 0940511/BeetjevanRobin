---
title: Hoorcolleges Aantekeningen
subtitle: Design Challenge 1, Game Design
date: 2017-10-23
categories: ["hoorcolleges"]
tags: ["hoorcollege", "ontwerpproces","verbeelden","prototyping", "onderzoeken", "Designchallenge"]
---

Wat is een game?

“Play is a free activity standing quite consciously outside ‘ordinary’ life as being ‘not serious’” ~ Huizinga.
Volgens Huizinga moet gamen een doel hebben en is het aangeboren gedrag. Iedereen kan spelen vanaf geboorte, zowel dieren als mensen spelen uit zichzelf.

“A game is a system in which players engage in artificial conflict defined by rules results in quantificial outcomes.”  ~ Zimmerman.
Waar bestaat een game nu eigenlijk uit? Een game ontstaat alleen wanneer het een doel heeft, regels heeft en er keuzes mogelijk zijn. Dit bij elkaar vormt de gameplay. Gameplay comes first! (Gouden regel).

Hoe het komt dat wij mensen games spelen komt doordat we het leuk vinden. Een game wordt leuk wanneer er een bepaalde ervaring aan vast zit. Een ervaring krijg je alleen wanneer er een betekenis is en dit geeft weer context, wat de ervaring weer relevant maakt om te spelen. Ervaringen bij games = Meaningful Play (mp). MP maakt games leuk.

DOEL bij games.

MP ontstaat door een duidelijk doel te stellen voor spelers.  WTF (Where’s the fun?)

KEUZES bij games.

Actie = reactie ~ Isaac Newton.
MP ontstaat door merkbare resultaten uit de acties van spelers. Voorbeelden zijn; level omhoog; punten scoren (heel basic); audio feedback; visuele feedback.

Risico = als beloning > geen succes.
Risico > dan beloning > geen succes.
Risico < dan beloning > Succes.

Alle games moeten worden afgestemd op de verschillende game types.

REGELS bij games.

Regels beperken de speler zijn acties.
Regels zijn expliciet en eenduidig.
Regels behoren toe aan elke speler.
Regels staan vast.
Regels moeten zich herhalen in het spel.
Regels zijn bindend.
Regels bestaan alleen in de ‘magic circle’
Magic circle geeft een tijd en plaats aan waar de regels gelden.

Alternitif reality > Pokémon go.

####TIPS
1. WTF
2. Kies de juiste tool.
3. Fouten = goed.
4. Meer tijd maakt het niet altijd beter.
5. Beperk je kaders.
6. Parallelle ontwikkelingen, meerdere games tegelijkertijd.
7. Fake it till you make it.
8. Kill your darlings. Als het niet leuk is, gooi het weg.
9. Gooi alles overboord, werkt het dan nog en is het leuk, dan is het goed! > Goed variëren
10. Na 20% van de beschikbare doorlooptijd van het ontwikkelde traject moet het eerste prototype getest worden.

**Content = Koning, Testen = Aas!**

