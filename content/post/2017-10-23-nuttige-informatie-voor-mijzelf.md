---
title: Nuttige Informatie
subtitle: Eigenlijk alleen voor mijzelf :)
date: 2017-03-05
categories: ["informatie"]
tags: ["informatie", "nuttig", "docenten", "kwartaaldoelen", "Tools for design", "keuzevakken"]
---

SC > Gerhard Rekveld

PD > Elske Rever

verdieping 0 > stadslab: printen op A1 etc. en figuurtjes hout laseren.

10 weken per kwartaal, week 7 tentamen, week 10 herkansing.

Keuzevakken: Di > 17:00-18:40 of 18:40-20:20.
					  Do > 8:30-10:10
`Starten periode 2.`

Tools for design beginnen ....? 
###Studiepunten
**60 punten totaal te verdienen; **

48 punten nodig voor jaar 2

60 punten nodig voor Propedeuse

2 punten voor   personal challenge

4 punten voor Design Theory (hoor/werk colleges)

9 punten voor Design Challenge

Nul-metingen voor Tekenen, Engels en Nederlands.

Voor je tentamen
 --> ID meenemen
 --> schoolpasje meenemen
 --> spiekbriefje van A4
 --> kwartier van te voren aanwezig.



