---
title: Deliverables
subtitle: De deliverables van de eerste design challenge
date: 2017-10-23
categories: ["Design Challenge 1"]
tags: ["design challenge 1", "deliverables"]
---

####Inventaris team:

Dominique – 0949537 > komt van havo, 17. Kwaliteiten;

Puck – 0945203 > komt van havo, 18. Kwaliteiten;

Quinty – 0949997 > komt van havo, 17. Kwaliteiten;

Robin Lisa – 0940511 > komt van vwo, 19. Kwaliteiten;

Serhat – 0951074 > komt van mbo studie, 19. Kwaliteiten;

####Team afspraken/doelen:

Veel communiceren (via app, face to face)
Op tijd aangeven dat je de door ons gestelde deadline niet haalt, dan kan de rest helpen.
Delen van info, via schoolmail. Zelf beslissen wat je wilt hebben en wat niet.
Outlook app downloaden en inloggen op schoolmail, zodat niemand informatie of meldingen mist.
Eind van de week ‘vergadering’ houden om te overleggen en een indicatie hebben van.
3 dagen voor de deadline alles af hebben zodat, de laatste drie dagen puntjes op de ‘i’ gezet kunnen worden.
####Planning en taakverdeling:

Wat?	Wie?	Wanneer ?
Spullen verzamelen voor moodboard over gekozen doelgroep en informatie voor onderzoeksrapport.	Iedereen individueel (thuis)	Voor Woensdag 6 september 2017 af.
Moodboard af en alles informatie verzameld en verwerkt hebben, onderzoek visueel gemaakt.	Iedereen individueel (school&thuis)	Op Woensdag 6 september 2017 af voor de derde niet ingeroosterde dag (waarschijnlijk vrijdag 9 september 2017)
‘Dit ben Ik’- profiel aanmaken	Iedereen individueel (thuis)	Af voor dinsdag 12 september 2017
Paperprotoype voor eigen idee af	Iedereen individueel (thuis& School)	Af op  zondag 8 september 2017
Prototype voor team af	Iedereen samen (school & misschien buiten school)	Af voor dinsdag 12 september 2017
Spelanalyse, hoe werkt het spel?	Iedereen (school)	Op Woensdag 13 september 2017 af.
Presentatie maken van het spel	Iedereen (school)	Af voor vrijdag 15 september 2017
Blog bijwerken per dag	Iedereen individueel	Af vrijdag 15 september 2017 (voor 17:00!!)
Alle hierboven genoemde dingen inleveren	Iedereen (wat individueel moet en als team)	Voor 17:00 vrijdag 15 september 2017 af.
Vergadering om vorderingen te checken	Iedereen (thuis) online vergadering?	Elke zondag rond 20:00/20:30.
 

####Taakverdeling;

**WIE?**	**WAT?**
Dominique		nog te bepalen
Puck	Teamcaptain, check up.
Quinty		nog te bepalen
Robin	Upload alle teamopdrachten.
Serhat		nog te bepalen
 

*In de beginfases zonder dat we nog een concreet groepsidee hebben zijn er maar 2 taken te verdelen. Wie doet de presentatie en wie doet de spelanalyse à taken verdelen. Steeds specifieker werken is belangrijker. Dat maakt het een stuk makkelijker.*

###Onderzoek uitwerkingen + Conclusies.
** De doelgroep** houdt van afwisseling, het bedenken van leuke en creatieve oplossingen. 

 Een geschikt spel voor eerste jaars CMD'ers is iets waar dingen bedacht moeten worden op een leuke manier, zoals vlot bouwen. Veel afwisseling.
 Over Rotterdam willen de meeste studenten alle in's en out's weten. Bijvoorbeeld; "Hoe werkt het ov? Waar kan ik allemaal uit? Bij welke winkels en/of andere faciliteiten krijg ik met mijn studentenkaart korting? Waar zitten welke restaurantjes, welke soort restaurantjes zijn er allemaal?" 
 De meeste mensen willen zoveel mogelijk mensen ontmoeten, het liefste de ene dag de ene helft. Dan de tweede dag de andere helft zodat de derde dag echt een goede keuze gemaakt kan worden. Via een database later zelf iedereen nog een keer rustig opzoeken vonden de meeste ook wel een fijn idee. Om toch nog even te zien; hoe ze heette, leeftijd interesses etc. Sommige leek het ook wel leuk om zelf dingen toe te kunnen voegen bij mensen om ze zo makkelijker te kunnen onthouden. Zoals "Kan goed tekenen" 
**Eindgoal** Het kennen van Rotterdam en de andere eerste jaars, beloning is niet heel nodig, kunnen winnen is vaak genoeg *(er moet dus een punten systeem komen)* , soms een gratis ijsje of koffie/thee is ook een leuk idee. Gratis studiepunt is niet reëel haalbaar. 

**Doel:** Zoveel mogelijk te weten te komen over Rotterdam, over je medestudenten en vooral iedereens kwaliteiten. En ook hoe het schoolgebouw in elkaar zit en alles werkt.

****Het eerste spel idee:**** *(concept)*

**Het Rotterdamse thema** ; Waarvoor gekozen is, is Amusement, waarbij we gaan kijken naar uitgaan, eten, winkelen, maar ook een beetje de HR op de wijnhaven en het ov.

**Voor het online aspect** is de bedoeling dat gegevens kunnen worden uitgewisseld, door middel van liever dit of dat vragen en door de persoonlijke vragenlijst die aan het begin van de game worden gehouden. ( *die offline ingevuld kunnen worden en uit eindelijk wel online opgestuurd en gedeeld, maar ook offline bekeken kunnen worden* ). De totaal score van de punten die iedereen haalt is ook online, net als het grootste gedeelte van het spel.

Er komen **vragen** over Rotterdam, gericht op het thema of vragen om elkaar beter te leren kennen. > je wisselt van groepje (door de pc gemaakt) naar een andere locatie.

**Het verbinden** gaat dus door middel van 'liever dit of dit' vragen, de vragenlijst en dan kunnen zien hoeveel je matched met de andere personen in de database en doordat je met z'n tweeën of drieën verder loopt naar het volgende punt. Hierdoor heb je een soort van speeddaten.

**Door het spel te spelen leer je Rotterdam dus kennen** , dit komt doordat je het spel speelt in Rotterdam zelf (de binnenstad, OV en de wijnhaven locatie).

**Tijdens de game** worden eenmaal op locatie aangekomen de opdrachten in het echt (offline aspect) door peercoaches uitgelegd en gespeeld of (online aspect) door het spel zelf uitgelegd en gespeeld.

**Verdere offline aspecten** zijn avatar aanmaken en aanpassen, de vragenlijst invullen, door je database gaan en je eigen profiel aanpassen.

**De game** moet leuk worden door middel van de afwisseling in online/offline/reallife; groepswisseling en leuke challenges met af en toe een tijdslimiet die het stresslevel van de nieuwe studenten moet gaan testen.  Het is de bedoeling dat iedereen op de app blijft zitten en dus niet online antwoorden gaat zoeken of routes via Google maps. De routes worden dan ook in de app uitgestippeld. Er komt een punten systeem per persoon, omdat er zo vaak gewisseld wordt.  Dit is natuurlijk niet 'high priorty', maar voor de fanatiekelingen wel leuk. 

Er zijn weinig **keuzes** en je moet altijd aan een opdracht meedoen, kan niet kiezen de opdracht niet te doen. consequenties bij goed of fout zal echter alleen punten aftrek worden bij een fout antwoord of niet binnen de tijd. Tijdslimiet is bij sommige spellen aanwezig. 

**De magic circle** van het spel zal worden de binnenstad van Rotterdam, OV en de wijnhaven. 

###Eerste versie prototype