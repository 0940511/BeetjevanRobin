---
title: STARRT
subtitle: starrt's van de eerste design challenge
date: 2017-10-23
categories: ["Design challenge 1"]
tags: ["STARRT","competenties", "CMD"]
---

STARRTS

***Competentie samenwerken*** 

 
Situatie
De eerste iteratie was bijna ten einde en alle deliverables moesten worden ingeleverd. Iedereen had zijn eigen taak, waarbij ze dingen af moesten maken, zodat het paperprototype op tijd af zou zijn en we de deadline zouden halen. Deze taken hadden we netjes verdeeld en op papier staan. 

Taak
Mijn taak was dat ik ervoor zou zorgen dat het eerste paperprototype van onze eerste game een lopend verhaal zou worden, in een logische volgorde. En een paar schermen maken voor in de telefoon die de route en een minigame moesten uitbeelden. 

Actie
Nadat ik dus wist wat ik moest doen ging ik aan de slag met mijn taak. Echter werd ik een beetje iets te enthousiast en begon niet alleen het paperprototype in de goede volgorde te leggen. Ik ging ook de delen die andere mensen hadden gemaakt opnieuw maken en verbeteren. Hierdoor kreeg ik het bijna niet op tijd af waardoor ik mijn groepje moest inschakelen voor het helpen van het volbrengen mijn taak.

Resultaat
Mijn groepje was uiteindelijk erg teleurgesteld in mij en vond dat ik te veel deed en daardoor soms andere hun werk overbodig werd omdat ik het al had gedaan. Hierdoor was ik bang dat ik de volgende keer weer te veel zou doen en mijn groepje weer boos zou worden. Ik heb hierdoor bij de volgende iteraties mijzelf heel erg teruggetrokken opgesteld zodat ik niet weer ‘te veel’ zou doen, ten nadele van mijn groepje en onze samenwerking. Als reactie hierop vond mijn groepje aan het einde van iteratie 3 dat ik geen initiatief toonde.

Reflectie
Achteraf gezien had ik beter kunnen vragen wat mijn groepje bedoelde met te veel doen en mijzelf niet veranderen. Doordat ik van heel veel naar bijna niks ben gegaan, heeft mijn groepje het gevoel gehad dat ik niks wilde doen of niks deed. Ik vond het heel naar om te horen dat ik geen initiatief getoond heb in de ogen van mijn groepje en wil dat dan ook zeker uit de weg gaan.

Transfer
De volgende design challenge zal ik meer initiatief gaan tonen, maar tegelijkertijd er ook op letten dat ik niet het werk van iemand anders overdoe, tenzij zij hierom vragen. Ik zal ook actiever zijn in het vragen of ik iets kan doen, zodat ik de samenwerking in mijn nieuwe groepje goed kan laten werken. 
 

***Competentie samenwerken***

 
Situatie
Gedurende de gehele design challenge had ons groepje last van opstart problemen.
Taak
Mijn taken waren eigenlijk onbekend en vaak onduidelijk, we hadden niet echt goede planningen en wisten alleen dat de deliverables ingeleverd en gemaakt moesten worden. Dit was eigenlijk de overkoepelende taak. 

Actie
Doordat alles zo langzaam van start gingen en niemand, vooral in het begin, echt wist wat we moesten doen en vooral hoe we het moesten doen, deden we vrij weinig. Hierdoor begon ik me te vervelen, dit ten nadele van mijn groepje en onze samenwerking. Ik maakte namelijk dingen niet af of bleef als het al iets slechter met me ging vanwege mijn ziekte daadwerkelijk thuis. Hierdoor verslechterde de band met mij en mijn groepje heel erg.  Hierdoor voelde ik me steeds meer buitengesloten en begon ook steeds minder voor het groepje te doen en andersom kreeg ik steeds meer het gevoel dat mijn groepje niet begreep hoe ik me voelde als het slecht met me ging. Hierdoor kregen we vaak meningsverschillen wat leidde tot het feit dat ik niet meer werd bijgepraat als ik iets had gemist. En dan ook niet goed meer wist wat er moest gebeuren, tot ergernis van onze teamcaptain die op de dag van de deadlines altijd erg veel stress ervaarde omdat naar haar idee nog niks compleet af was. 

Resultaat
Als resultaat hierop kwam dat iedereen in het groepje steeds meer irritaties ontwikkelde en er drie kampen ontstonden. Hierdoor was het vaak 1 tegen 3 en durfde ik uiteindelijk mijn mening en mijn stem niet meer te laten horen. Ook hadden we elke dag een dag planning nodig, omdat door gebrek aan communicatie iedereen het overzicht kwijtraakte van wat er nou gedaan was en wat niet. Dit leidde vaak tot grote frustraties als de helft van het groepje wel dacht dat iets af was en de andere dacht of vond van niet. Ook waren we niet allemaal eerlijk over het feit of we af hadden wat we individueel hadden moeten maken voor het team. Al met al was door dit alles de samenwerking erg stroef en liep het aan het einde vaak uit tot heftige discussies, tot huilen aan toe. 

Reflectie
Ik denk dat we achteraf gezien veel eerder eerlijker tegenover elkaar hadden moeten zijn en iedereen had zijn kaarten op tafel moeten leggen en niet zoals nu kampen moet en vormen. Als we dit hadden gedaan en begrip voor elkaars minderheden hadden getoond denk ik dat alles veel soepeler en makkelijker zou zijn verlopen en waarschijnlijk hadden we dan een nog mooier resultaat gehad dan we nu hadden. Ik had beter met mijn groepje moeten communiceren en moeten aangeven wat me dwars zat.

Transfer
De volgende keer zal ik gelijk eerlijk zijn en zeggen wat ik denk. Ook zal ik minder proberen te missen en dingen op tijd afmaken en naar wens van mijn groepje. Ook hoop ik op een beter taakverdeling zodat iedereen beter op de hoogte is wat af is en wat niet en dat iedereen dan ook eerlijk is over of het af is of niet. 
 

***Competentie professionaliseren ***

 
Situatie
We kregen tijdens de studio coaching uitleg en daarna moesten we zelf een Dit-ben-ik-profiel maken, met daarin in meerdere eisen. 

Taak
Ik moest op zoek naar mijn stijl en mijn ik-profiel moest voldoen aan verschillende eisen die we tijdens de les hadden opgesteld. 

Actie
Ik ben opzoek gegaan naar voorbeelden van cv’s van designers en heb gekeken wat ik mooi vond. Hierna heb ik verschillende dingen uitgeprint en uitgeknipt wat ik mooi vond aan de cv’s.  Zo kreeg ik een indicatie van wat ik wilde op een moodboard. Hierna ben ik aan de slag gegaan met alle onderdelen die verplicht waren op een word documentje neer te zetten op de manier van wat ik daarvoor had verzameld op mijn moodboard. Zo kreeg ik een kale versie van mijn leerdossier. Als laatste heb ik kleuren toegevoegd die ik heel mooi vond staan bij elkaar en heb foto’s toegevoegd. Toen was mijn dit-ben-ik-profiel helemaal af en naar mijn wens. Als laatste heb ik gekeken of ik daadwerkelijk alle eisen in mijn profiel had verwerkt. 

Resultaat
Ik was best tevreden met hoe mijn profiel er uiteindelijk uit is komen te zien en vond het ook erg fijn dat ik een goed beeld had van wat ik wilde hebben. Hierdoor kreeg ik echt wat ik in gedachten had op papier. Ook komt het goed over op andere mensen wie ik ben en wat ik kan. Het is een goed resultaat als het gaat om communiceren, wat ook het doel was van dit profiel.

Reflectie
Ik denk dat mijn profiel nog mooier was geworden als ik een ander programma had gebruikt dan Word. Ik ben nog niet helemaal tevreden en dat komt omdat ik sommige balkjes niet recht achter het woord kreeg zoals ik wilde. Waarschijnlijk was het wel gelukt in het programma Adobe Indesign, maar daar ben ik nog niet zo gevorderd in, dit zou ik wel nog beter kunnen doen.

Transfer
De volgende keer als ik weer zoiets moet maken, al is het een website of visitekaartjes weet ik precies hoe ik het moet gaan aanpassen en wat ik wel en niet handig en mooi vind. Ook weet ik nu precies hoe ik goed kan communiceren met anderen om te laten zien wie ik ben, met zulke eenvoudige dingen als eigen stijl. 


***Competentie professionaliseren***

 
Situatie
De Expositie kwam eraan en we moesten aan iedereen gaan vertellen waar ons spel overging en hoe het in zijn werk ging. 

Taak
Aan mij de taak om als allereerste de pitch te doen en iedereen uitleg geven. 

Actie
Ik moest als allereerste bij ons spel blijven staan om iedereen uitleg te geven en alle vragen te beantwoorden. Het enge was dat ik van tevoren nog niet wist wat ik kon verwachten, dit zorgde ervoor dat ik me voelde alsof ik onvoorbereid een presentatie moest geven. Desondanks ging het er wel makkelijk van af. Ik wist op elke vraag wel antwoord te geven, dit kwam natuurlijk doordat ik zoveel van ons spel afwist en goed geïnformeerd en overlegd had met het team.  Ook doordat we alles hadden getest konden we zo uitgebreid mogelijk antwoord geven.

Resultaat
Uiteindelijk ging het erg goed. Ik kreeg steeds meer zelfvertrouwen en het praatje ging steeds soepeler. Ik kreeg veel complimentjes en verbeterpunten zonder dat ik het had gevraagd, maar ook kreeg ik veel feedback van mensen als ik ze vroeg om een feedbackformulier in te vullen. Veel mensen waren juist door mijn verhaal heel enthousiast geworden dat ons spel veel rode stickers in ontvangst mocht nemen.

Reflectie
Door de feedback die ik zowel mondeling als op papier heb gekregen, gaf aan dat ik het overigens erg goed heb gedaan. Het enige puntje van kritiek dat ik nog moet verbeteren is dat ik nogal snel praat en daardoor mensen soms de draad van mijn verhaal kwijtraakten. Dit heeft er waarschijnlijk dan ook voor gezorgd dat ik niet alle rode stickers kreeg die ons spel had kunnen krijgen, maar verder heb ik het verhaal goed over kunnen brengen op het publiek volgens de feedback, ik maakte genoeg oogcontact en gaf mensen volgens de feedback echt het gevoel alsof mijn pitch naar hun gericht was.

Transfer 
Voor de volgende keer wil ik er dus op gaan letten dat ik rustiger praat, maar wel zo dat het niet mijn enthousiasme weghaalt. Verder wil ik nog steeds even goed geïnformeerd zijn als ik nu was zodat ik weer op elke vraag wel een antwoord weet. 
 
 
***Competentie professionaliseren***

 
Situatie
Voor de design challenge moesten we meerdere prototypes maken. En om te beginnen in iteratie 1 en eerste paperprototype.

Taak
 Mijn taak was om verschillende dingen te maken voor het eerste paperprototype. 

Actie 
Omdat ik nog niet zo heel veel wist over prototyping en zowel voor mezelf en voor het team meer wilde weten, ben ik de Workshop Prototyping gaan volgen. Hierbij moest je 4 keer een middag komen en voor de workshop zou beginnen moest je elke les een stukje lezen dat we toe gemaild kregen. Het lezen duurde ongeveer een uurtje, dit was in het Engels wat erg leerzaam was en verder was het ook best interessant. Ik vond de workshop daarentegen wel wat tegenvallen. Ik had verwacht dat we meer theorie zouden krijgen en minder zouden knutselen zoals er nu gebeurde. Echter waren de boekjes die we kregen waar de opdrachten instonden wel weer vol nuttige informatie. Dingen die ik heb geknutseld zijn onder andere een draagbare koelkast om mee te nemen op vakantie. 

Resultaat
Het resultaat van het volgen van de workshop was dat ik meer wist over wat je allemaal kon doen voordat je daadwerkelijk een prototype ging maken en dat je op verschillende manieren bij hetzelfde doel uit kan komen. Verder ben ik er ook achter gekomen dat deze opleiding meer doen is dan leren, wat ik erg jammer vind en tegelijkertijd ook weer leuk. Ik moet alleen nog even omschakelen van boeken leren naar via mijn handen leren. 

Reflectie
Ik ben erg blij dat ik de workshop gevolgd heb en kan hierdoor elke keer nog even de tekst raadplegen die we opgestuurd hebben gekregen als ik het niet meer weet of als ik vastloop. Hierdoor heb ik bij volgende dingen daadwerkelijk een vooronderzoekje gedaan, zodat ik dat kan uitleggen en kan beargumenteren waarom ik iets heb gekozen als prototype. 

Transfer
Als ik nog een keer een prototype moet maken weet ik veel beter waar ik moet beginnen door alle kennis die ik heb verzameld en zal er dus in het vervolg strenger en kritischer naar kijken zodat ik nog beter kan worden in het maken van een prototype. Ik zal ook nog meer verschillende soorten willen uitproberen om erachter te komen welke uiteindelijk het beste past in bepaalde situaties.  

***Competentie inrichten ontwerpproces***

 
Situatie
In iteratie drie hebben we nieuwe concepten bedacht om tot een beter spel te komen. Uiteindelijk moesten we er 1 kiezen die we verder gingen uitwerken.
Taak
Mijn taak was het helpen van een beslissing maken door goede argumenten te geven waarom het ene wel ging en het andere niet. 

Actie
Mijn team had al 3 concepten zonder mij gekozen, dit omdat ik wat later was wegens mijn ziekte. Hierdoor kon ik alleen nog meedoen in het proces over welk van de drie concepten we zouden gaan kiezen. Ik vond dat alle drie de spellen best wel leuk waren, maar bij alle drie zaten ook evenveel voor als nadelen als het ging om het vermaken van een grote groep tieners en kiezen zou daarom dan ook erg moeilijk worden. Ik stelde voor om feedback te vragen, zo konden we erachter komen wat de docenten vonden en nieuwe inzichten konden krijgen. Met de feedback konden we afwegen wat we zouden gaan doen en welke dingen we weg konden laten. Uiteindelijk kwamen we op het idee om alle drie de concepten door te voeren door ze samen te voegen en er 1 geheel van te maken.

Resultaat
Door de feedback en het bekijken van wat wel en niet kan en wat de mogelijkheden zijn, konden we dingen wegstrepen en uiteindelijk op een goed resultaat uitkomen wat leuk zou zijn als spel voor de doelgroep en wat paste bij de vraag van de opdrachtgever. 

Reflectie
Door alle afwegingen te bekijken en goed na te denken over wat praktisch was en op tijd feedback te vragen konden we tot een mooi resultaat komen met een nieuw game concept. 

Transfer
De volgende keer zou ik er graag bij willen zijn bij het bedenken van de concepten zodat ik daarna nog beter onderzoek kan doen en afwegen wat het beste past bij de gevraagde eisen van de opdrachtgever. 

***Competentie inrichten ontwerpproces***

 
Situatie
Voor iteratie 3 moesten we, nadat we feedback van de alumni hadden gekregen ervoor zorgen dat de minigames waren uitgedacht en paste in het verhaal van het spel. 

Taak
Mijn taak was om 1 minigame te bedenken en een link naar het verhaal van het spel te leggen. 

Actie
Ik ben opzoek gegaan naar offline spelletjes waarbij je veel moest nadenken, aangezien ik het spelletje een beetje uitdagend wilde maken en niet heel gemakkelijk of zo maken dat het snel saai zou worden. Hier kwam in met mijn onderzoek achter dat veel leuke puzzelachtige spelletjes in het Nintendospel ‘Professor Layton’ zaten. Ik ben het spel gaan spelen en bij leuke puzzels heb ik ze opgeschreven. Nadat ik een aardig ideetje had van wat ik zelf wilde maken ben ik aan de slag gegaan. Zo kwam ik op een spelletje waarbij je blokken moet verschuiven om het document van boven naar beneden te krijgen. Ik heb daarna elementen toe gevoegd en een verhaaltje bedacht zodat de minigame aansluiting had met het hoofdspel. 

Resultaat
Ik vond het spel erg goed gelukt. Nadat ik het helemaal uitgewerkt had heb ik dan ook een spelanalyse uitgevoerd en het spel getest. De mensen die het hebben gespeeld waren erg positief en fanatiek. Ze vonden het erg leuk dat je zo goed moest nadenken, maar ook dat je gewoon elke keer opnieuw kon beginnen. 

Reflectie
De volgende keer zou ik vooraf nog willen onderzoeken wat de doelgroep van de andere mogelijke spelletjes vond, zodat ik echt nog kon kijken welke het beste aansloot bij de doelgroep. Dit zou echter wel veel tijd in beslag nemen, dus moet daar wel een aangelegenheid voor zijn. 

Transfer
Als ik het nog een keer zou doen, zou ik het graag sneller aanpakken. Wel weet ik nu dat het goed werkt als je zelf gewoon dat gaat doen wat je moet ontwerpen, zoals in dit geval spelletjes spelen. Zo kon je er perfect achter wat werkt en wat niet en kun je daarna er je eigen draai aan geven. 
 

***Competentie inrichten ontwerpproces***

 
Situatie
In iteratie drie moesten we weer een ontwerpproceskaart maken. Ik had dit nog nooit gedaan, want alle keren daarvoor was ik er niet bij geweest vanwege mijn ziekte.

Taak
Mijn taak was om alle stukjes tekst waar iets op stond wat we deze iteratie hadden gedaan uit te knippen, zodat we de ontwerpproceskaart konden gaan maken 

Actie
Ik ben niet zomaar alles gaan uitknippen want dat vond ik erg zonde van onze tijd. Eerst heb ik goed gekeken naar het voorbeeld op het papier van de ontwerpproceskaart. Toen ik een beetje een idee had van wat er gedaan moest worden, ben ik zorgvuldig gaan kijken naar alle mogelijkheden die op het blad stonden. Toen ik wist wat er allemaal opstond ben ik begonnen met knippen. Zo heb ik uiteindelijk alle dingen uitgeknipt die we daadwerkelijk hebben gedaan. En als ik niet wist wat iets was heb ik het eerst gevraagd zodat we niet iets zouden missen, omdat ik niet wist wat het was. 

Resultaat
Uiteindelijk waren we toch nog best wel snel klaar met onze ontwerpproceskaart maken en ik had zelfs te veel dingen uitgeknipt die we toch niet hadden gedaan. 

Reflectie
Toch had ik graag nog een extra uitleg gehad over waarom we dit moeste doen, maar waarschijnlijk had ik er gewoon bij moeten zijn toen het werd uitgelegd.

Transfer
De volgende keer ga ik nog beter vragen wat alles is, zodat ik niet dingen te veel ga doen terwijl ik dan wat anders had kunnen doen. Ook wil ik me geen zorgen maken over dat iets veel tijd kost, maar gewoon zeker weten dat ik weet wat alles betekend en wat er allemaal bedoeld wordt. 
 
