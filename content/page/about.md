---
title: Robin Lisa de Rooij
subtitle: Personal Page
date: 2017-10-23
categorie: ["personal page"]
tags: ["ik", "mezelf","personal"]
---

*Hello bloglezers,
  ik ben Robin Lisa, maar gewoon alleen Robin is voldoende. Ik vind het ontwerpen van bepaalde dingen om situaties te verbeteren erg leuk en verder vind ik het leuk om kaartjes, menukaarten en websites te ontwerpen. Ik heb ook andere hobby’s, deze zijn Schrijven, ik schrijf mijn eigen fictional verhaal, daarbij vind ik het ook erg leuk om fantasy verhalen te lezen, creatief bezig zijn en ik houd heel erg van sporten en met name volleybal. 
Ik vind het leuk om moeilijke uitdagingen aan te gaan en samen met andere tot een goed idee te komen. Ik ben bereid om tot het uiterste te gaan en streef graag en soms a bit too much naar perfection.*

<b>*Studievoortgang*</b>  
  <u>Kwaliteiten:</u>  
  - leergierig  
  - snel lerend  
  - kritisch  
  - perfectionistisch  
  - communicatief sterk  
  - harde werker  
  - creatief  

<u>Kwartaaldoelen:</u>  
  - goede websites maken  
  - beter kunnen plannen  
  - nog sneller kunnen werken  
  - eigen stijl ontwikkelen  

<u>Keuzevakken:</u>  
  - Creatief schrijven  
  - 7 eigenschappen die je succesvol maken  
  - bijspijkercursus frans  

<u>keuzenvakken *op de wachtlijst*:</u>  
  - Festival management  
  - Mooiere presentaties+ verslagen maken  

<u>Tools for Design:</u>  
  - Indesign  
  - Illustrator  
  - Sketch  
  - Photoshop  

<u>Personal Challenges:</u>  
  - Via karakter test de beste planning krijgen.  


*Hopefully I see you around!*

*Lots of Love*

*R L*♕